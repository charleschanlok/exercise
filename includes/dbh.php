<?php

$db["default"] = array(
    "hostname" => "localhost",
    "username" => "root",
    "password" => "charles",
    "database" => "exercise",
);

$conn = mysqli_connect(
    $db["default"]["hostname"],
    $db["default"]["username"],
    $db["default"]["password"],
    $db["default"]["database"]
);
<?php

session_start();

if (isset($_POST['submit'])) {

    include 'dbh.php';

    $uid      = mysqli_real_escape_string($conn, $_POST['uid']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    //Error handler
    //Check for empty field
    if (empty($uid) || empty($password)) {
        header("Location: ../index.php?login=empty");
        exit();
    } else {
        $sql          = "SELECT * FROM users WHERE user_uid='$uid' OR user_email='$uid'";
        $result       = mysqli_query($conn, $sql);
        $result_check = mysqli_num_rows($result);

        if ($result_check < 1) {
            header("Location: ../index.php?login=error");
            exit();
        } else {
            if ($row = mysqli_fetch_assoc($result)) {
                //Compare the password
                $hashed_password_check = password_verify($password, $row['user_pwd']);
                if ($hashed_password_check === false) {
                    header("Location: ../index.php?login=error");
                    exit();
                } elseif ($hashed_password_check === true) {
                    //Log in the user
                    $_SESSION['user_id']        = $row['user_id'];
                    $_SESSION['user_firstname'] = $row['user_firstname'];
                    $_SESSION['user_lastname']  = $row['user_lastname'];
                    $_SESSION['user_email']     = $row['user_email'];
                    $_SESSION['user_uid']       = $row['user_uid'];
                    header("Location: ../views/dashboard.php");
                    exit();
                }
            }
        }
    }
} else {
    header("Location: ../index.php?login=error");
    exit();
}

<?php

if (isset($_POST["submit"])) {
    include_once 'dbh.php';

    //Get the users' data and sanitize it
    $first_name = mysqli_real_escape_string($conn, $_POST["first_name"]);
    $last_name  = mysqli_real_escape_string($conn, $_POST["last_name"]);
    $email      = mysqli_real_escape_string($conn, $_POST["email"]);
    $uid        = mysqli_real_escape_string($conn, $_POST["uid"]);
    $password   = mysqli_real_escape_string($conn, $_POST["password"]);

    //Error handler
    //Check for empty field
    if (empty($first_name) || empty($last_name) || empty($email) || empty($uid) || empty($password)) {
        header("Location: ../signup.php?signup=empty");
        exit();
    } else {
        //Check if input characters ara valid
        if (!preg_match("/^[a-zA-Z]*$/", $first_name) ||
            !preg_match("/^[a-zA-Z]*$/", $last_name)) {
            header("Location: ../signup.php?signup=invalid");
            exit();
        } else {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                header("Location: ../signup.php?signup=invalid_email");
                exit();
            } else {
                $sql          = "SELECT * from users WHERE user_id = '$uid'";
                $result       = mysqli_query($conn, $sql);
                $result_check = mysqli_num_rows($result);

                if ($result_check > 0) {
                    header("Location: ../signup.php?signup=usertaken");
                    exit();
                } else {
                    //Hash the password
                    $hashed_password = password_hash($password, PASSWORD_DEFAULT);
                    // Insert the user into the database
                    $sql = "INSERT INTO users (user_firstname, user_lastname, user_email, user_uid, user_pwd)
                    VALUES
                    ('$first_name', '$last_name','$email', '$uid', '$hashed_password');";
                    $result       = mysqli_query($conn, $sql);
                    header("Location: ../signup.php?signup=success");
                    exit();
                }
            }
        }
    }

} else {
    header("Location: ../signup.php");
    exit();
}
